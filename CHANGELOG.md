# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--
## [Unreleased]
-->

## [0.1.1] - 2019-11-04
### Changed
- Dockerfile: Entrypoint removed, default shell changed to `bin/sh`

## [0.1.0] - 2019-11-04
### Added
- Dockerfile created
- README.md is created
- CHANGELOG.md is created
- LICENSE.md is added
- gitlab-ci.yml is added

<!--
## [0.0.0] - 2017-06-20
### Added
### Changed
### Fixed
### Removed
-->

<!--
[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...HEAD
-->
[0.1.1]: https://gitlab.com/cli-tool-containers/cfssl/tags/0.1.0
[0.1.0]: https://gitlab.com/cli-tool-containers/cfssl/tags/0.1.0
