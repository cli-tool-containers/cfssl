FROM golang:1.13.3-alpine3.10 as builder
WORKDIR /workdir
RUN set -x && apk --no-cache add git gcc libc-dev make && git clone https://github.com/cloudflare/cfssl.git /workdir && git clone https://github.com/cloudflare/cfssl_trust.git /etc/cfssl && make clean && make bin/rice && ./bin/rice embed-go -i=./cli/serve && make all

FROM alpine:3.10
COPY --from=builder /etc/cfssl /etc/cfssl
COPY --from=builder /workdir/bin/ /usr/bin
EXPOSE 8888
CMD ["/bin/sh"]
