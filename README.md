# CFSSL Container

This repository contains a dockerfile to use the `cfssl` and `cfssljson` CLI tools inside a container. 

## **TL;DR**
Clone this repository and run:
```bash
docker build -t cfssl . && docker run --rm --entrypoint=cfssl cfssl version
```

## How-To

A container including Cloudflare's PKI and TLS toolkit. Instead of installing and maintaining a CLI tool on you environment, you can use this container to create your certificate authorities and certificates.

### Build

Build the docker container for `cfssl` and `cfssljson`:

```bash
docker build -t cfssl .
```

### Run

Then use the container image like this:
```bash
docker run --rm --entrypoint=cfssl cfssl <your-cfssl-command>
```

Or change the entrypoint to, for example, `cfssljson`:
```bash
docker run --rm --entrypoint=cfssljson cfssl <your-cfssljson-command>
```

Additionally you can give the container access to the current directory:
```bash
docker run --rm --entrypoint=cfssl -v $(pwd):/current-directory -w="/current-directory" cfssl <your-cfssl-command>
```

### Tips&Tricks

You can add the following line to your `.bash_profile`:
```bash
alias cfssl="docker run --rm --entrypoint=cfssl -v $(pwd):/current-directory -w="/current-directory" cfssl"
```

## Versioning

For the versions available, see the [tags on this repository](https://gitlab.com/cli-tool-containers/cfssl/tags) or the [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Michiel Bakker** - *Owner/maintainer* - [GitLab profile](https://gitlab.com/mvbakker)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Cloudflare's CFSSL GitHub](https://github.com/cloudflare/cfssl)
* [This README template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2) by PurpleBooth
